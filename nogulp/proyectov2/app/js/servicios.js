jQuery(document).ready(function () {
    $(".accordion .btn-link").click(function () {
        setTimeout(function () {
            $(".accordion .collapse").each(function (index) {
                var btn = $(this).parent().find(".btn-link");
                btn.removeClass("active");
                if ($(this).hasClass("show")) {
                    btn.addClass("active");
                }
            });
        }, 500);
    });
    $('.carousel-item>.wrapper').zoom({ on: 'click' });	
});