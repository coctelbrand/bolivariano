
  	
  	jQuery(document).ready(function(){

  		var LocsA = [
		    {
		    	lat:12.5518826,
		    	lon:-81.7163086,
		    	title:'Bolivariano',
		    	html: ['<h3>Expreso</h3>'].join(''),
		    	zoom: 11,	
		    	icon:'images/misc/pin_map.png',
		    	animation:google.maps.Animation.DROP
		    },

		    {
		    	lat:10.833306,	 
		    	lon:-75.0146484,	
				title: 'Bolivariano',
				html: ['<h3>Expreso</h3>'].join(''),
				zoom: 11,
				icon: 'images/misc/pin_map.png',
				animation: google.maps.Animation.DROP
		    },

		    {
		    	lat:6.5336451,	 
		    	lon:-75.3662109,	
				title: 'Bolivariano',
				html: ['<h3>Expreso</h3>'].join(''),
				zoom: 11,
				icon: 'images/misc/pin_map.png',
				animation: google.maps.Animation.DROP
			}
			
		];


		var styles = {

		    'Ultracem': [{
		        featureType: 'all',
		        stylers: [
		            { saturation: -100 },
		            { gamma: 0.50 }
		        ]
		    }],
		    
		    'Ultracem': [
		        {
		            "featureType": "road",
		            "elementType": "geometry",
		            "stylers": [
		                {
		                    "visibility": "off"
		                }
		            ]
		        },
		        {
		            "featureType": "poi",
		            "elementType": "geometry",
		            "stylers": [
		                {
		                    "visibility": "off"
		                }
		            ]
		        },
		        {
		            "featureType": "landscape",
		            "elementType": "geometry",
		            "stylers": [
		                {
		                    "color": "#fffffa"
		                }
		            ]
		        },
		        {
		            "featureType": "water",
		            "stylers": [
		                {
		                    "lightness": 50
		                }
		            ]
		        },
		        {
		            "featureType": "road",
		            "elementType": "labels",
		            "stylers": [
		                {
		                    "visibility": "off"
		                }
		            ]
		        },
		        {
		            "featureType": "transit",
		            "stylers": [
		                {
		                    "visibility": "off"
		                }
		            ]
		        },
		        {
		            "featureType": "administrative",
		            "elementType": "geometry",
		            "stylers": [
		                {
		                    "lightness": 40
		                }
		            ]
		        }
		    ]
		};

		var styled = new Maplace({
	      map_div: '#gmap',
	      locations: LocsA,
	      start: 1,
	      styles: styles,
	      map_options: {
	          zoom: 8
	      }
	    });

	    styled.Load();
	    
	});
  